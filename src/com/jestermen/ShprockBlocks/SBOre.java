//
//		This class represents the falling pieces of ore in "Shprock Blocks."
//		-- Author: Adam Testerman
//

package com.jestermen.ShprockBlocks;

public class SBOre
{
	//PROPERTIES / Variables
	private int x;				//screen coordinates of this ore
	private int y;
	private int width = 32;		//graphical dimensions of this ore
	private int height = 32;
	private int speedY;			//vertical speed of this ore; modified by GRAVITY constant
	private boolean isSettled;	//determines when this ore is settled and when the next ore will start falling
	
	
	//=================================
	//		VITAL METHODS
	//=================================
	
	/**
	 * Startup.
	 */
	public static void main(String[] args)
	{
		SBOre ore = new SBOre();
	}
	
	
	/**
	 * Constructor. Creates a new instance of SBOre.
	 */
	public SBOre()
	{
		
	}
	
	
	//=================================
	//		PUBLIC METHODS
	//=================================
		
	/**
	 * Game loop update. Called once per frame.
	 */
	public void update()
	{
		speedY += SBMain.GRAVITY;
		
		y += speedY;
		
		//Bounds checking
		if (y + height > SBMain.SCREEN_HEIGHT - 32)
		{
			y = SBMain.SCREEN_HEIGHT - height - 32;
			
			speedY = 0;
			isSettled = true;
		}
	}
	
	
	//=================================
	//		ACCESSOR METHODS
	//=================================
	
	public boolean isSettled()
	{
		return isSettled;
	}

	public int getX()
	{
		return x;
	}

	public void setX(int x)
	{
		this.x = x;
	}

	public int getY()
	{
		return y;
	}

	public void setY(int y)
	{
		this.y = y;
	}

	public int getWidth()
	{
		return width;
	}

	public void setWidth(int width)
	{
		this.width = width;
	}

	public int getHeight()
	{
		return height;
	}

	public void setHeight(int height)
	{
		this.height = height;
	}
}