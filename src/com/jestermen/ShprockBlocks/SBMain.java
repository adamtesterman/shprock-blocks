//
//		This is the main class for "Shprock Blocks."
//		-- Author: Adam Testerman
//

package com.jestermen.ShprockBlocks;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.net.URL;
import java.util.Random;

import com.jestermen.framework.Animation;

public class SBMain extends Applet implements Runnable
{
	//PROPERTIES / Constants
	public static final int SCREEN_WIDTH =		480;	//screen resolution
	public static final int SCREEN_HEIGHT =		854;
	public static final int GRAVITY = 			1;		//speed that objects fall (pixels per frame)
	private final int FPS = 					30;		//desired frames per second for this app
	private final int NUM_COLS = 				8;
	private final int NUM_ORE =					10;		//total number of ore that will be created
	private final int CONVEYOR_X =				SCREEN_WIDTH / 2 - 128;
	private final int CONVEYOR_Y =				SCREEN_HEIGHT - 32;
	
	//PROPERTIES / Variables
	private int numCreatedOre;
	
	//PROPERTIES / Objects
	private SBOre fallingOre;
	private Image image;
	private Image gfxOre;
	private Image gfxBG;
	private Image gfxConveyor;
	private Image gfxHeli00;
	private Image gfxHeli01;
	private Image gfxHeli02;
	private Image gfxHeli03;
	private Graphics second;
	private Animation anim;
	
	//PROPERTIES / Lists
	private SBOre[] ore;
	
	
	//=================================
	//		VITAL METHODS
	//=================================
	
	/**
	 * Startup.
	 */
	public static void main(String[] args)
	{
		
	}
	
	
	//=================================
	//		OVERRIDE METHODS
	//=================================
	
	@Override
	public void init()
	{
		//Initialize variables
		ore = new SBOre[NUM_ORE];
		
		//Setup application frame
		setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
		setBackground(Color.BLACK);
		setFocusable(true);
		Frame frame = (Frame) this.getParent().getParent();
		frame.setTitle("Shprock Blocks");
		
		//Setup images
		URL baseURL = getDocumentBase();
		gfxOre = getImage(baseURL, "data/ore.png");
		gfxBG = getImage(baseURL, "data/bg.png");
		gfxConveyor = getImage(baseURL, "data/conveyor.png");
		gfxHeli00 = getImage(baseURL, "data/heli00.png");
		gfxHeli01 = getImage(baseURL, "data/heli01.png");
		gfxHeli02 = getImage(baseURL, "data/heli02.png");
		gfxHeli03 = getImage(baseURL, "data/heli03.png");
		
		//Setup animations
		anim = new Animation();
		anim.addFrame(gfxHeli00, 100);
		anim.addFrame(gfxHeli01, 100);
		anim.addFrame(gfxHeli02, 100);
		anim.addFrame(gfxHeli03, 100);
	}
	
	@Override
	public void update(Graphics g)
	{
		if (image == null)
		{
			image = createImage(this.getWidth(), this.getHeight());
			second = image.getGraphics();
		}
		
		second.setColor(getBackground());
		second.fillRect(0, 0, getWidth(), getHeight());
		second.setColor(getForeground());
		paint(second);
		
		g.drawImage(image, 0, 0, this);
	}
	
	@Override
	public void paint(Graphics g)
	{
		g.drawImage(gfxBG,  0,  0,  this);
		g.drawImage(gfxConveyor, CONVEYOR_X, CONVEYOR_Y, this);
		
		SBOre thisOre;
		for (int i = 0; i < numCreatedOre; i++)
		{
			thisOre = ore[i];
			g.drawImage(gfxOre, thisOre.getX(), thisOre.getY(), this);
		}
		
		g.drawImage(anim.getImage(), 100, 100, this);
	}

	@Override
	public void start()
	{
		//Create the first ore and wait for it to fall before making subsequent ores
		createOre();
		
		Thread thread = new Thread(this);
		thread.start();
	}

	@Override
	public void stop()
	{
		
	}

	@Override
	public void destroy()
	{
		
	}
	
	/**
	 * Contains game loop.
	 */
	@Override
	public void run()
	{
		//Game loop
		while (true)
		{
			animate();
			repaint();
			
			try
			{
				Thread.sleep(1000/FPS);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public void animate()
	{
		anim.update(10);
	}
	
	
	//=================================
	//		PRIVATE METHODS
	//=================================
	
	/**
	 * Creates an ore if the ore pool is not empty and positions it at a random x-coordinate above the conveyor.
	 */
	private void createOre()
	{
		if (numCreatedOre < ore.length)
		{
			ore[numCreatedOre] = fallingOre = new SBOre();
			
			int randCol = new Random().nextInt(NUM_COLS);
			fallingOre.setX(CONVEYOR_X + randCol * 32);
			
			numCreatedOre++;
		}
	}
}